# Safety App Development

Install Node.js 10, Install Android Studio and follow the tutorial here: https://facebook.github.io/react-native/docs/getting-started.html

Install Expo command line:
> npm install -g expo-cli
in the project folder:
>npm install
>npm start

Build for ios: > expo build:ios
>expo publish

Run in Android emulator:
start your emulator
> npm run android

Expo eject will be able to create project structure for both Android and iOS native apps. You can edit setting for the apps

The dependencies for the project can be found at: package.json
